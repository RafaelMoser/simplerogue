

import UI.SwingUICore;
import UI.SwingUiIntegration;
import core.GameEngine;
import core.interfaces.UiConnector;

public class Main {

	public static void main(String[] args) {
		GameEngine d = new GameEngine(60,30);
		SwingUICore s = new SwingUICore();
		
		UiConnector u = new SwingUiIntegration(d, s);
		d.setUIConnector(u);
		s.setUIConnector(u);
		
		s.createMainWindow(800,600);
		u.sendVisualPackage(d.getVisualPackage());
	}

}
