package UI.Interfaces;

import core.interfaces.GameCore;

public interface UI {

	void createMainWindow(GameCore game);
	void update(String map);

}
