package UI;

import java.awt.event.KeyEvent;

import core.Coordinate;
import core.GameEngine;
import core.enums.Inputs;
import core.interfaces.UiConnector;
import core.visualPackage.VisualPackage;

public class SwingUiIntegration implements UiConnector{

	private GameEngine d;
	private SwingUICore s;
	
	public SwingUiIntegration(GameEngine d, SwingUICore s) {
		this.d = d;
		this.s = s;
	}

	@Override
	public void sendVisualPackage(VisualPackage p) {
		for (Coordinate c : p.dun.items.keySet()) {
			p.dun.dungeonMap[c.getX()][c.getY()] = p.dun.items.get(c);
		}
		for (Coordinate c : p.dun.enemies.keySet()) {
			p.dun.dungeonMap[c.getX()][c.getY()] = p.dun.enemies.get(c);
		}
		p.dun.dungeonMap[p.pc.coord.getX()][p.pc.coord.getY()] = p.pc.charRepresentation;
		
		String visual = "Turn: "+p.turnCount;
		while(visual.length()<p.dun.dungeonMap[0].length) {
			visual+=" ";
		}
		visual += "  Game Log:\n";
		for (int i = 0; i < p.dun.dungeonMap.length; i++) {
			visual += new String(p.dun.dungeonMap[i]);
			if(p.log.size()>i) {
				visual+= "  "+p.log.get(i).toString();
			}
			visual +="\n";			
		}
		visual+="\n";
		visual+="Player level:"+p.pc.level+"	Hp:"+p.pc.hp+"/"+p.pc.maxHp+"	Exp:"+p.pc.exp+"/"+p.pc.nextLevel+"\n";
		visual+="Inventory:\n";
		for (int i = 0; i < p.pc.inventory.size(); i++) {
			if(i!=0 && i!=5)visual+="	";
			if(i==5)visual+="\n";
			if(i==p.pc.selectedItem) {
				visual += "["+p.pc.inventory.get(i)+"]";
			} else {
				visual += " "+p.pc.inventory.get(i);
			}
		}
		visual+="\nEffect: "+p.pc.selectedDescription;
		
		s.update(visual);
	}

	@Override
	public void sendInput(int inputCode) {
		Inputs i = null;
		switch (inputCode) {
		case KeyEvent.VK_NUMPAD8: 
			i = Inputs.UP;
			break;
		case KeyEvent.VK_NUMPAD9: 
			i = Inputs.UPRIGHT;
			break;
		case KeyEvent.VK_NUMPAD6: 
			i = Inputs.RIGHT;
			break;	
		case KeyEvent.VK_NUMPAD3: 
			i = Inputs.DOWNRIGHT;
			break;	
		case KeyEvent.VK_NUMPAD2: 
			i = Inputs.DOWN;
			break;
		case KeyEvent.VK_NUMPAD1: 
			i = Inputs.DOWNLEFT;
			break;
		case KeyEvent.VK_NUMPAD4: 
			i = Inputs.LEFT;
			break;
		case KeyEvent.VK_NUMPAD7: 
			i = Inputs.UPLEFT;
			break;
		case KeyEvent.VK_NUMPAD5: 
			i = Inputs.STOP;
			break;
		case KeyEvent.VK_ADD:
			i = Inputs.INVENTORYDOWN;
			break;
		case KeyEvent.VK_SUBTRACT:
			i = Inputs.INVENTORYUP;
			break;
		case KeyEvent.VK_NUMPAD0:
			i = Inputs.IVENTORYUSE;
		default:
			break;
		}
		if(i!= null) d.inputEvent(i);
	}

}
