package UI;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JTextPane;

import core.interfaces.UiConnector;

public class SwingUICore implements KeyListener {

	private UiConnector ui;
	private JTextPane p;
	
	public void createMainWindow(int width, int height) {
		JFrame frame = new JFrame("SimpleRogue");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setPreferredSize(new Dimension(width, height));
		p = new JTextPane();
		p.setFocusable(false);
		p.setFont(new Font("consolas", Font.PLAIN, 12));
		frame.add(p);
		frame.pack();
		frame.setLocationRelativeTo(null); 
		frame.setVisible(true);
		frame.addKeyListener(this);
		
		
	}
	
	public void setUIConnector(UiConnector u) {
		this.ui = u;
	}
	public void update(String map) {
		p.setText(map);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		ui.sendInput(e.getKeyCode());
	}
	@Override
	public void keyReleased(KeyEvent e) {}
	
	@Override
	public void keyTyped(KeyEvent e) {}
}
