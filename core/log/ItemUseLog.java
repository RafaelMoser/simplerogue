package core.log;

import core.interfaces.GameLog;

public class ItemUseLog extends BaseLog implements GameLog{

	private final String itemName;
	
	public ItemUseLog(int turn, String itemName) {
		super(turn);
		this.itemName = itemName;
	}
	
	@Override
	public String toString() {
		return super.toString()+itemName+" used";
	}
}
