package core.log;

import core.interfaces.GameLog;

public class GameOverLog extends BaseLog implements GameLog{
	
	public GameOverLog(int turn) {
		super(turn);
	}

	@Override
	public String toString() {
		return super.toString()+"Game over! Press 5 to restart";
	}

}
