package core.log;

import core.interfaces.GameLog;

public class KillLog extends BaseLog implements GameLog {

	private final String enemyName;
	private final int xp;
	
	public KillLog(String enemyName, int xp, int turn) {
		super(turn);
		this.enemyName = enemyName;
		this.xp = xp;
	}
	
	@Override
	public String toString() {
		if(xp!=0) return super.toString()+enemyName+" died, +"+xp+" exp";
		return super.toString()+enemyName+" died";
	}
}

