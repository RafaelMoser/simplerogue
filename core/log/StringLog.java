package core.log;

import core.interfaces.GameLog;

public class StringLog extends BaseLog implements GameLog {

	private final String message;
	
	public StringLog(int turn, String message) {
		super(turn);
		this.message = message;
	}

	@Override
	public String toString() {
		return super.toString()+message;
	}
}
