package core.log;

import core.interfaces.GameLog;

public class AttackLog extends BaseLog implements GameLog {
	
	public final String attacker;
	public final String target;
	public final int damage;
	public final int targetHp;
	
	public AttackLog(int turn, String attacker, String target, int damage, int targetHp) {
		super(turn);
		this.attacker = attacker;
		this.target = target;
		this.damage = damage;
		this.targetHp = targetHp;
	}
	
	@Override
	public String toString() {
		return super.toString()+"["+attacker+"]->["+target+"]: "+damage+" damage ("+targetHp+" hp)";
	}

}
