package core.log;

import core.interfaces.GameLog;

public class ItemPickLog extends BaseLog implements GameLog {

	private final String itemName;
	
	public ItemPickLog(int turn, String itemName) {
		super(turn);
		this.itemName = itemName;
	}
	
	@Override
	public String toString() {
		return super.toString()+itemName+" obtained";
	}
	
}
