package core.log;

import core.interfaces.GameLog;

public class LadderLog extends BaseLog implements GameLog {

	public LadderLog(int turn) {
		super(turn);
	}
	
	@Override
	public String toString() {
		return super.toString()+"Moved down the stairs";
	}
}
