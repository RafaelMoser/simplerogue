package core.log;

import core.interfaces.GameLog;

public abstract class BaseLog implements GameLog {
	
	private final int turn;
	
	public BaseLog(int turn) {
		this.turn = turn;
	}

	@Override
	public String toString() {
		return "{"+turn+"}";
	}
}
