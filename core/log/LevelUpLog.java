package core.log;

import core.interfaces.GameLog;

public class LevelUpLog extends BaseLog implements GameLog{

	int level;
	
	public LevelUpLog(int level, int turn) {
		super(turn);
		this.level = level;
	}
	
	@Override
	public String toString() {
		return super.toString()+"Player level up! Current level: "+level;
	}
}
