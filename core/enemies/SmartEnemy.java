package core.enemies;

import core.Coordinate;
import core.DungeonMap;
import core.entities.BasicCharacter;
import core.enums.Direction;
import core.interfaces.Enemy;
import core.interfaces.Entity;
import core.log.AttackLog;

public class SmartEnemy extends BasicCharacter implements Enemy{
	
	public static int totalCount = 0;
	private final int id;

	public SmartEnemy(int level, Coordinate startingPos) {
		super(startingPos);
		this.level = level;
		this.currentHp = level*2;
		SimpleEnemy.totalCount += 1;
		
		this.id = totalCount;
		this.name = "Hobgoblin "+id;
		this.charRepresentation = 'H';
	}

	@Override
	public AttackLog attack(Entity target, int turn) {
		int targetHp = target.takeDamage(level);
		return new AttackLog(turn, this.getName(), target.getName(), this.level, targetHp);
	}

	@Override
	public Direction act(DungeonMap d) {
		return AStar.Search(currentPos, d.getPCPosition(), d);
	}
	
	@Override
	public int getExp() {
		return 20*level;
	}
}