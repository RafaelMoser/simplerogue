package core.enemies;

import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import core.Coordinate;
import core.DungeonMap;
import core.enums.Direction;

public class AStar {

	public static Direction Search(Coordinate start, Coordinate goal,  DungeonMap map) {

		SortedSet<Node> openList = new TreeSet<>();
		List<Node> closedList = new LinkedList<>();
		openList.add(new Node(start, 0, h(start,goal), null));
		while(!openList.isEmpty()) {
			Node current = openList.first();
			if(current.c.equals(goal)) {
				return getDirection(current);
			}
			openList.remove(current);
			closedList.add(current);
			for (Node n : neighbors(current, map)) {
				if(!closedList.contains(n)) {
					n.f = n.g+h(n.c,goal);
					if(openList.contains(n)) {
						for(Node openNode : new LinkedList<Node>(openList)) {
							if(openNode.equals(n)) {
								if(n.g<openNode.g) {
									openList.remove(openNode);
									openList.add(n);
								}
							}
						}
					} else {
						openList.add(n);
					}
				}
			}
			
		}
		return Direction.relativeDirection(start, goal);
	}
	
	private static double h(Coordinate s, Coordinate g) {
		return Math.sqrt(Math.pow((s.getX()-g.getX()),2) + Math.pow((s.getY()-g.getY()),2));
	}
	
	private static List<Node> neighbors(Node o, DungeonMap m){
		List<Node> n = new LinkedList<>();
		for (Direction d : Direction.values()) {
			Coordinate c = o.c.move(d); 
			
			if(!c.equals(o.c) && m.getCell(c).isWalkable()) {
				n.add(new Node(c, o.g+1, 0, o));
			}
		}
		return n;
	}
	
	private static Direction getDirection(Node n) {
		Node prev = n;
		while(n.parent!= null) {
			prev = n;
			n = n.parent;
		}
		return Direction.relativeDirection(prev.parent.c, prev.c);
	}
}

class Node implements Comparable<Node>{

	Coordinate c;
	double g;
	double f;
	Node parent;
	
	public Node(Coordinate c, double g, double f, Node parent) {
		super();
		this.c = c;
		this.g = g;
		this.f = f;
		this.parent = parent;
	}

	@Override
	public int compareTo(Node o) {
		return Double.compare(this.f, o.f);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!(obj instanceof Node)) return false;
		Node n = (Node)obj;
		return c.equals(n.c);
	}
	
	@Override
	public int hashCode() {
		return c.hashCode();
	}
	
}