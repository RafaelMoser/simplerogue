package core.cells;

import core.GameEngine;
import core.interfaces.Cell;
import core.interfaces.Enemy;

public class Wall implements Cell{

	@Override
	public boolean isWalkable() {
		return false;
	}
	
	@Override
	public void onEnemyStep(GameEngine g, Enemy target) {}
	
	@Override
	public void onPcStep(GameEngine g) {}

	
	@Override
	public char getCharRepresentation() {
		return '#';
	}

}
