package core.cells;

import core.GameEngine;
import core.interfaces.Cell;
import core.interfaces.Enemy;

public class Trap implements Cell {

	private boolean isKnown;
	private int damage;
	
	public Trap(int damage) {
		this.isKnown = false;
		this.damage = damage;
	}
	
	@Override
	public boolean isWalkable() {
		return true;
	}

	@Override
	public void onEnemyStep(GameEngine g, Enemy target) {
		g.damageEnemy("Trap", target, damage);
		isKnown = true;
	}
	
	@Override
	public void onPcStep(GameEngine g) {
		g.damagePc("Trap", damage);
		isKnown = true;
	}


	@Override
	public char getCharRepresentation() {
		return isKnown?'x':' ';
	}

}
