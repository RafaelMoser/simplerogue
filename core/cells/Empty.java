package core.cells;

import core.GameEngine;
import core.interfaces.Cell;
import core.interfaces.Enemy;

public class Empty implements Cell {

	@Override
	public boolean isWalkable() {
		return true;
	}

	@Override
	public void onEnemyStep(GameEngine g, Enemy target) {}
	
	@Override
	public void onPcStep(GameEngine g) {}

	@Override
	public char getCharRepresentation() {
		return ' ';
	}
}
