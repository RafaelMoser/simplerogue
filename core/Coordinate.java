package core;

import core.enums.Direction;

public class Coordinate {

	private final int x;
	private final int y;
	
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Coordinate(Coordinate c) {
		this.x = c.x;
		this.y = c.y;
	}
	
	public Coordinate move(Direction d) {
		return new Coordinate(x+d.x,y+d.y);
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!(obj instanceof Coordinate)) return false;
		Coordinate c = (Coordinate)obj;
		return (c.x == this.x && c.y == this.y);
	}
	
	@Override
	public int hashCode() {
		int result = x;
		result = 31 * result + y;
		return result;
	}
}
