package core.entities;

import java.util.LinkedList;
import java.util.List;

import core.Coordinate;
import core.enums.Inputs;
import core.interfaces.Entity;
import core.interfaces.Item;
import core.items.SmallPotion;
import core.log.AttackLog;
import core.log.ItemUseLog;
import core.visualPackage.PcVisualPackage;

public class Player extends BasicCharacter implements Entity{

	private int currentXp;
	private List<Item> inventory;
	private int selItem;
	
	public Player(int level, Coordinate startingPos) {
		super(startingPos);
		this.level = level;
		this.currentHp = level*5;
		this.inventory = new LinkedList<>();
		this.inventory.add(new SmallPotion());
		this.inventory.add(new SmallPotion());
		this.selItem = 0;
		
		this.currentXp = 0;
		
		this.name = "You";
		this.charRepresentation = '@';
		
	}
	
	public void changeSelectedItem(Inputs d) {
		if(d == Inputs.INVENTORYUP && selItem >0) {
			selItem--;
		}
		if(d == Inputs.INVENTORYDOWN && selItem <inventory.size()-1) {
			selItem++;
		}
	}
	
	public void setPosition(Coordinate c) {
		currentPos = c;
	}
	
	public ItemUseLog useItem(int turn) {
		if(inventory.isEmpty()) {
			return null;
		}
		inventory.get(selItem).use(this);
		ItemUseLog log = new ItemUseLog(turn, inventory.get(selItem).getItemName());
		inventory.remove(selItem);
		if(selItem>= inventory.size()) {
			selItem=inventory.size();
		}
		return log;
	}
	
	public void pickItem(Item i) {
		inventory.add(i);
	}
	
	public boolean isInventoryFull() {
		return inventory.size()==10;
	}
	
	@Override
	public AttackLog attack(Entity target, int currentTurn) {
		int targetHp = target.takeDamage(level);
		return new AttackLog(currentTurn, this.getName(), target.getName(), level, targetHp);
		
	}
	
	public void heal(int amount) {
		currentHp += amount;
		if(currentHp > level*5) currentHp = level*5;
	}
	
	public boolean gainXp(int xp){
		currentXp += xp;
		if(currentXp<level*50) {
			return false;
		}
		currentXp -= level*50;
		level++;
		currentHp = level*5;
		return true;
	}
	
	public int getLevel() {
		return level;
	}

	public int getCurrentHp() {
		return currentHp;
	}
	
	public PcVisualPackage getVisualPackage() {
		PcVisualPackage p = new PcVisualPackage();
		p.coord = currentPos;
		p.charRepresentation = charRepresentation;
		p.level = level;
		p.hp = currentHp;
		p.maxHp = level*5;
		p.exp = currentXp;
		p.nextLevel = level*50;
	
		p.inventory = new LinkedList<>();
		for (Item item : inventory) {
			p.inventory.add(item.getItemName());
		}
		p.selectedItem = selItem;
		if(inventory.isEmpty()) 
			p.selectedDescription = "";
		else
			p.selectedDescription = inventory.get(selItem).getEffectDescription();
		
		return p;
	}
}
