package core.entities;

import core.Coordinate;
import core.enums.Direction;
import core.interfaces.Entity;

public abstract class BasicCharacter implements Entity {

	protected Coordinate currentPos;
	protected int level;
	protected int currentHp;
	protected String name;
	protected char charRepresentation;

	public BasicCharacter(Coordinate startingPos) {
		this.currentPos = startingPos;
	}
	
	@Override
	public Coordinate getPosition() {
		return new Coordinate(this.currentPos);
	}

	@Override
	public void move(Direction d) {
		this.currentPos = this.currentPos.move(d);
	}
	
	@Override
	public int takeDamage(int damage) {
		currentHp -= damage;
		if(currentHp <=0) currentHp = 0;
		return currentHp;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public char getCharRepresentation() {
		return charRepresentation;
	}
}
