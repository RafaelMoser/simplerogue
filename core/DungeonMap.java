package core;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import core.entities.Player;
import core.interfaces.Cell;
import core.interfaces.Enemy;
import core.interfaces.Item;
import core.visualPackage.DungeonVisualPackage;

public class DungeonMap {
	
	private Player pc;
	private Cell[][] map;
	private Map<Coordinate, Enemy> enemies;
	private Map<Coordinate, Item> items;

	
	
	public DungeonMap(Player pc, int level, int width, int height) {
		this.pc = pc;
		pc.setPosition(new Coordinate(3,3));
		this.map = MapGenerator.generateMap(width, height, level);
		this.enemies = MapGenerator.generateEnemies(map, level);
		this.items = MapGenerator.generateItems(map, level);
	}

	public Cell getCell(Coordinate c) {
		return map[c.getX()][c.getY()];
	}
	
	public boolean isCellMovable(Coordinate c) {
		return getCell(c).isWalkable()
				&& enemies.get(c) == null 
				&& !pc.getPosition().equals(c);
	}
	
	public Collection<Enemy> getEnemies(){
		return enemies.values();
	}
	
	public boolean cellContainsItem(Coordinate c) {
		return items.containsKey(c);
	}
	
	public boolean cellContainsEnemy(Coordinate c) {
		return enemies.containsKey(c);
	}
	
	public Item removeItem(Coordinate c) {
		return items.remove(c);
	}
	
	public Enemy getEnemy(Coordinate c) {
		return enemies.get(c);
	}
	
	public Enemy removeEnemy(Coordinate c) {
		return enemies.remove(c);
	}
	
	public void moveEnemy(Coordinate from, Coordinate to) {
		enemies.put(to, enemies.remove(from));
	}
	
	public DungeonVisualPackage getVisualPackage() {
		DungeonVisualPackage pack = new DungeonVisualPackage();
		pack.dungeonMap = new char[map.length][map[0].length];
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[i].length; j++) {
				pack.dungeonMap[i][j] = map[i][j].getCharRepresentation();
			}
		}
		pack.enemies = new HashMap<>();
		for (Enemy e : enemies.values()) {
			pack.enemies.put(e.getPosition(), e.getCharRepresentation());
		}	
		pack.items = new HashMap<>();
		for (Coordinate c : items.keySet()) {
			pack.items.put(c, items.get(c).getCharRepresentation());
		}
		
		return pack;
	}

	public Coordinate getPCPosition() {
		return pc.getPosition();
	}
}
