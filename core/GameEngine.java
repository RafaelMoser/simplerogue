package core;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import core.enemies.SimpleEnemy;
import core.enemies.SmartEnemy;
import core.entities.Player;
import core.enums.Direction;
import core.enums.Inputs;
import core.interfaces.Enemy;
import core.interfaces.GameLog;
import core.interfaces.Item;
import core.interfaces.UiConnector;
import core.log.AttackLog;
import core.log.GameOverLog;
import core.log.ItemPickLog;
import core.log.ItemUseLog;
import core.log.KillLog;
import core.log.LadderLog;
import core.log.LevelUpLog;
import core.log.StringLog;
import core.visualPackage.VisualPackage;

public class GameEngine {
	
	int level;
	private int turnCount;
	private List<GameLog> log;
	private UiConnector ui;
	
	private DungeonMap map;
	private Player pc;
	
	int height;
	int width;
	
	public GameEngine(int width, int height) {
		this.width = width;
		this.height = height;
		startGame();
	}
	
	private void startGame() {
		this.level = 1;
		this.turnCount = 0;
		this.log = new LinkedList<>();
		this.pc = new Player(1, new Coordinate(3,3));
		
		SimpleEnemy.totalCount=0;
		SmartEnemy.totalCount=0;
		map = new DungeonMap(pc, level, width, height);

		log.add(new StringLog(0, "Control with numpad"));
		log.add(new StringLog(0, "1-9: directions, attack by moving into an enemy"));
		log.add(new StringLog(0, "0: use item, +/-: select item"));
	}
	
	public void setUIConnector(UiConnector u) {
		this.ui = u;
	}
	
	public void inputEvent(Inputs i) {
		if(pc.getCurrentHp()==0) {
			if(i == Inputs.STOP) {
				startGame();
				ui.sendVisualPackage(getVisualPackage());
			}
			return;
		}
		boolean turnAction = false;
		boolean inventoryAction = false;
		switch (i) {
		case UP:
			turnAction = movePc(Direction.NORTH);
			break;
		case UPRIGHT:
			turnAction = movePc(Direction.NORTHEAST);
			break;
		case RIGHT:
			turnAction = movePc(Direction.EAST);
			break;
		case DOWNRIGHT:
			turnAction = movePc(Direction.SOUTHEAST);
			break;
		case DOWN:
			turnAction = movePc(Direction.SOUTH);
			break;
		case DOWNLEFT: 
			turnAction = movePc(Direction.SOUTHWEST);
			break;
		case LEFT:
			turnAction = movePc(Direction.WEST);
			break;
		case UPLEFT:
			turnAction = movePc(Direction.NORTHWEST);
			break;
		case STOP:
			turnAction = true;
			break;
		case IVENTORYUSE:
			turnAction = useItem();
			break;
		case INVENTORYDOWN:
		case INVENTORYUP:
			inventoryAction = true;
			pc.changeSelectedItem(i);
			break;
		default:
			break;
		}
		if(turnAction)	endTurn();
		if(inventoryAction) ui.sendVisualPackage(getVisualPackage());
	}
	
	private void endTurn() {
		turnCount++;
		for (Enemy e : new ArrayList<>(map.getEnemies())) {
			if(pc.getCurrentHp() !=0)
				moveEnemy(e, e.act(map));
		}
		ui.sendVisualPackage(getVisualPackage());
		
	}
	
	private boolean movePc(Direction d) {
		Coordinate c = pc.getPosition().move(d);
		if(map.isCellMovable(c)){
			pc.move(d);
			
			if(map.cellContainsItem(c)) {
				if(pc.isInventoryFull()) {
					log.add(new StringLog(turnCount, "Inventory full"));
				} else {
					Item i = map.removeItem(c);
					log.add(new ItemPickLog(turnCount, i.getItemName()));
					pc.pickItem(i);
				}
			}
			map.getCell(c).onPcStep(this);			
			return true;
		} else if (map.cellContainsEnemy(c)) {
			AttackLog a = pc.attack(map.getEnemy(c), turnCount);
			log.add(a);
			if(a.targetHp<=0) {
				Enemy e = map.removeEnemy(c);
				log.add(new KillLog(e.getName(), e.getExp(),turnCount));
				boolean lvlUp = pc.gainXp(e.getExp());
				if(lvlUp) {
					log.add(new LevelUpLog(pc.getLevel(), turnCount));
				}
			}
			return true;
		}
		return false;
	}
	
	private void moveEnemy(Enemy e, Direction d) {
		Coordinate c = e.getPosition().move(d);
		if(map.isCellMovable(c)){
			map.moveEnemy(e.getPosition(), c);
			e.move(d);
			map.getCell(c).onEnemyStep(this, e);
		} else if(pc.getPosition().equals(c)) {
			AttackLog a = e.attack(pc, turnCount);
			log.add(a);
			if(a.targetHp<=0) {
				log.add(new GameOverLog(turnCount));
			}
		}
	}
	

	public void damagePc(String source, int damage) {
		pc.takeDamage(damage);
		log.add(new AttackLog(turnCount, source, pc.getName(), damage, pc.getCurrentHp()));
		if(pc.getCurrentHp()<=0) {
			log.add(new GameOverLog(turnCount));
		}
	}
	
	public void damageEnemy(String source, Enemy target, int damage) {
		int eHp = target.takeDamage(damage);
		log.add(new AttackLog(turnCount, source, target.getName(), damage, eHp));
		if(eHp<=0) {
			map.removeEnemy(target.getPosition());
			log.add(new KillLog(target.getName(), 0,turnCount));
		}
	}
	
	public void nextFloor() {
		log.add(new LadderLog(turnCount));
		level++;
		map = new DungeonMap(pc, level, height, width);
	}
	
	private boolean useItem() {
		ItemUseLog i = pc.useItem(turnCount);
		if(i == null) {
			log.add(new StringLog(turnCount, "No items in inventory"));
			return false;
		}
		log.add(i);
		return true;
	}
	
	public VisualPackage getVisualPackage() {
		VisualPackage pack = new VisualPackage();
		pack.turnCount = turnCount;
		
		if(log.size()>height) { 
			pack.log = new ArrayList<GameLog>(log.subList(log.size()-height, log.size()));
		} else {
			pack.log = new ArrayList<GameLog>(log);
		}
		pack.dun = map.getVisualPackage();
		pack.pc = pc.getVisualPackage();
		
		return pack;
	}
}
