package core.visualPackage;

import java.util.List;

import core.Coordinate;

public class PcVisualPackage {
		//player info
		public Coordinate coord;
		public char charRepresentation;

		public int level;
		public int hp;
		public int maxHp;
		public int exp;
		public int nextLevel;
		
		public List<String> inventory;
		public int selectedItem;
		public String selectedDescription;
}
