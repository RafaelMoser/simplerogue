package core.visualPackage;

import java.util.Map;

import core.Coordinate;

public class DungeonVisualPackage {

	public char[][] dungeonMap;
	public Map<Coordinate, Character> enemies;
	public Map<Coordinate, Character> items;
}
