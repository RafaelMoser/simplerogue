package core.visualPackage;

import java.util.List;

import core.interfaces.GameLog;

public class VisualPackage {
	
	public int turnCount;
	public List<GameLog> log;
	
	public DungeonVisualPackage dun;
	public PcVisualPackage pc;
}