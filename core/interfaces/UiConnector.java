package core.interfaces;

import core.visualPackage.VisualPackage;

public interface UiConnector {
	
	void sendVisualPackage(VisualPackage p);
	
	void sendInput(int inputCode);

}
