package core.interfaces;

import core.GameEngine;

public interface Cell {

	boolean isWalkable();

	void onEnemyStep(GameEngine g, Enemy target);
	
	void onPcStep(GameEngine g);
	
	char getCharRepresentation();
}
