package core.interfaces;

import core.Coordinate;
import core.enums.Direction;

public interface Character {

	Coordinate getPosition();
	
	void move(Direction d);
}
