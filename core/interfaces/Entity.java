package core.interfaces;

import core.Coordinate;
import core.enums.Direction;
import core.log.AttackLog;

public interface Entity {

	Coordinate getPosition();
	
	void move(Direction d);
	
	AttackLog attack(Entity target, int turn);
	
	int takeDamage(int damage);
	
	char getCharRepresentation();
	
	String getName();
}
