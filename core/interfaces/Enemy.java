package core.interfaces;

import core.DungeonMap;
import core.enums.Direction;

public interface Enemy extends Entity{

	Direction act(DungeonMap d);
	
	int getExp();
}
