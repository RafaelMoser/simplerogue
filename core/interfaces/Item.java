package core.interfaces;

import core.entities.Player;

public interface Item {

	void pick(Player pc);

	void use(Player pc);
	
	char getCharRepresentation();
	
	String getItemName();
	
	String getEffectDescription();
}
