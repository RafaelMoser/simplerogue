package core.interfaces;

import core.enums.Inputs;

public interface GameCore {
	
	public void inputEvent(Inputs i);

}
