package core.items;

import core.entities.Player;
import core.interfaces.Item;

public class SmallPotion implements Item {

	@Override
	public void use(Player pc) {
		pc.heal(5);
	}

	@Override
	public void pick(Player pc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public char getCharRepresentation() {
		return '-';
	}

	@Override
	public String getEffectDescription() {
		return "Heals 5 HP";
	}

	@Override
	public String getItemName() {
		return "Small Potion";
	}
}
