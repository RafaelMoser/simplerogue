package core;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import core.cells.Empty;
import core.cells.Stairs;
import core.cells.Trap;
import core.cells.Wall;
import core.enemies.SimpleEnemy;
import core.enemies.SmartEnemy;
import core.interfaces.Cell;
import core.interfaces.Enemy;
import core.interfaces.Item;
import core.items.LargePotion;
import core.items.SmallPotion;

public class MapGenerator {
	
	public static Cell[][] generateMap(int x, int y, int level){
		Cell[][] map = new Cell[y][x];
		Random r = new Random();
		int type =  r.nextInt(3);
		boolean hasObstacle=true;
		int x1=0,x2=0,y1=0,y2=0;
		switch(type) {
		case 0:
			hasObstacle=false;
			break;
		case 1:
			y1 = r.nextInt(y/3)+5;
		case 2:
			x1 = r.nextInt(x/3)+5;
			x2 = x-5-r.nextInt(x/3);
			y2 = y-5-r.nextInt(y/3);
			break;
		}
		
		for(int i = 0; i <y; i++) {
			for (int j = 0; j < x; j++) {
				if(type !=2 && i == y-3 && j == x-3){
					map[i][j]=new Stairs();
				}else if(type ==2 && i == 2 && j == x-3){
						map[i][j]=new Stairs();
				} else if( i == 0 || i==y-1 || j==0 || j==x-1) {
					map[i][j]=new Wall();
				} else if (hasObstacle && i>y1 && i<y2 && j>x1 && j<x2) {
					map[i][j]=new Wall();
				} else {
					if(r.nextInt(1000)<level) {
						map[i][j]=new Trap(level);
					} else {
						map[i][j]=new Empty();
					}
				}
			}
		}
		return map;
	}
	
	public static Map<Coordinate, Enemy> generateEnemies(Cell[][] map, int level){
		Map<Coordinate, Enemy> enemies = new HashMap<>();
		int i = 0;
		Random r = new Random();
		while(i <= r.nextInt(3)+3){
			Coordinate c = new Coordinate(
					r.nextInt(map.length), 
					r.nextInt(map[i].length));
			if(c.getX()+c.getY()>10 //not inside safe zone
					&& map[c.getX()][c.getY()].isWalkable() 
					&& enemies.get(c) == null) {
				if(r.nextBoolean()) {
					enemies.put(c, new SimpleEnemy(level, c));
				} else {
					enemies.put(c, new SmartEnemy(level, c));
				}
				i++;
			}
		}
		
		return enemies;
	}
	
	public static Map<Coordinate, Item> generateItems(Cell[][] map, int level){
		Map<Coordinate, Item> items = new HashMap<>();
		int i = 0;
		Random r = new Random();
		while(i <= r.nextInt(3)+1){
			Coordinate c = new Coordinate(
					r.nextInt(map.length), 
					r.nextInt(map[i].length));
			if(c.getX()+c.getY()>10 //not inside safe zone
					&& map[c.getX()][c.getY()].isWalkable() 
					&& items.get(c) == null) {
				if(r.nextInt(10)<level) 
					items.put(c, new LargePotion());
				else
					items.put(c, new SmallPotion());
				i++;
			}
		}
		
		return items;
	}
	
}
