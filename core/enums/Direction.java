package core.enums;

import core.Coordinate;

public enum Direction {
	
	NORTH(-1,0),
	SOUTH(1,0),
	WEST(0,-1),
	EAST(0,1),
	NORTHWEST(-1,-1),
	NORTHEAST(-1,1),
	SOUTHWEST(1,-1),
	SOUTHEAST(1,1),
	WAIT(0,0);
	
	public final int x;
	public final int y;
	
	private Direction(int x,int y) {
		this.x = x;
		this.y = y;
	}
	
	public static Direction relativeDirection(Coordinate from, Coordinate to) {
		int x = (int) Math.signum(to.getX() - from.getX());
		int y = (int) Math.signum(to.getY() - from.getY());
		
		for(Direction d : Direction.values()) {
			if(d.x == x && d.y == y) {
				return d;
			}
		}
		return Direction.WAIT;
	}

}
